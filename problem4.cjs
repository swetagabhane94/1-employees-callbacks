const fs = require("fs");
const path = require("path");

const data = path.resolve("./data.json");
const result4 = path.resolve("./result4.json");

fs.readFile(data, "utf-8", (err, read) => {
  if (err) {
    console.error(`error occured in read file ${err}`);
  } else {
    console.log("Read sucessfully");
    let data = JSON.parse(read);
    let result = data.employees.filter((companyDetail) => {
      return companyDetail.id !== 2;
    });

    fs.writeFile(result4, JSON.stringify(result), (err) => {
      if (err) {
        console.error(`error occured in write file ${err}`);
      } else {
        console.log("write sucessfully");
      }
    });
  }
});
