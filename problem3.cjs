const fs = require("fs");
const path = require("path");

const data = path.resolve("./data.json");
const result3 = path.resolve("./result3.json");

fs.readFile(data, "utf-8", (err, read) => {
  if (err) {
    console.error(`error occured in read file ${err}`);
  } else {
    console.log("Read sucessfully");
    let data = JSON.parse(read);
    let result = data.employees.filter((companyDetail) => {
      return companyDetail.company === "Powerpuff Brigade";
    });

    fs.writeFile(result3, JSON.stringify(result), (err) => {
      if (err) {
        console.error(`error occured in write file ${err}`);
      } else {
        console.log("write sucessfully");
      }
    });
  }
});
