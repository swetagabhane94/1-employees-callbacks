const fs = require("fs");
const path = require("path");

const data = path.resolve("./data.json");
const result5 = path.resolve("./result5.json");

fs.readFile(data, "utf-8", (err, read) => {
  if (err) {
    console.error(`error occured in read file ${err}`);
  } else {
    console.log("Read sucessfully");
    let data = JSON.parse(read);
    let result = data.employees.sort((valueA, valueB) => {
      if (valueA.company === valueB.company) {
        return valueA.id - valueB.id;
      } else {
        if (valueA.company < valueB.company) {
          return -1;
        }
        if (valueA.company > valueB.company) {
          return 1;
        }
        return 0;
      }
    });

    fs.writeFile(result5, JSON.stringify(result), (err) => {
      if (err) {
        console.error(`error occured in write file ${err}`);
      } else {
        console.log("write sucessfully");
      }
    });
  }
});
