const fs = require("fs");
const path = require("path");

const data = path.resolve("./data.json");
const result1 = path.resolve("./result1.json");

fs.readFile(data, "utf-8", (err, read) => {
  if (err) {
    console.error(`error occured in read file ${err}`);
  } else {
    console.log("Read sucessfully");
    let data = JSON.parse(read);
    let result = data.employees.filter((companyDetail) => {
      if (
        companyDetail.id === 2 ||
        companyDetail.id === 13 ||
        companyDetail.id === 23
      ) {
        return companyDetail;
      }
    });

    fs.writeFile(result1, JSON.stringify(result), (err) => {
      if (err) {
        console.error(`error occured in write file ${err}`);
      } else {
        console.log("write sucessfully");
      }
    });
  }
});
